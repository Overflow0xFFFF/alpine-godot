FROM frolvlad/alpine-glibc:alpine-3.10
MAINTAINER Joshua Ford <joshua.ford@protonmail.com>

ENV GODOT_VERSION "3.1.1"
ENV PLATFORM "linux_x11_64"

RUN apk update && apk --no-cache add \
    ca-certificates \
    git \
    wget \
    unzip

RUN wget "https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip" \
    && unzip "Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip" \
    && mv "Godot_v${GODOT_VERSION}-stable_linux_headless.64" /usr/local/bin/godot \
    && wget "https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_export_templates.tpz" \
    && unzip "Godot_v${GODOT_VERSION}-stable_export_templates.tpz" \
    && mkdir -p "~/.local/share/godot/templates/${GODOT_VERSION}.stable" \
    && mv "templates/${PLATFORM}_debug" "~/.local/share/godot/templates/${GODOT_VERSION}.stable" \
    && mv "templates/${PLATFORM}_release" "~/.local/share/godot/templates/${GODOT_VERSION}.stable" \
    && rm -rf templates \
    && mkdir ~/.cache \
    && mkdir -p ~/.config/godot \
    && rm -f "Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip" \
    && rm -f "Godot_v${GODOT_VERSION}-stable_export_templates.tpz" \

